/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Abel
 * Created: 19 ene. 2022
 */

-- (1) listar las edades de los ciclistas (sin repetidos)

USE ciclistas;
SELECT DISTINCT edad FROM ciclistas;

-- (2) listar las edades de los ciclistas de Artiach

SELECT edad FROM ciclistas WHERE nomequipos="Artiach";

-- (3) listar las edades de los ciclistas de Artiach o de Amore Vita

SELECT edad FROM ciclistas WHERE nomequipos="Artiach" OR nomequipos="Amore vita";

-- (4) listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30

SELECT dorsal FROM ciclistas WHERE edad < 25 OR > 30;

-- (5) listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto

select dorsal from ciclistas where edad >28 and 32 group by nomequipo="Banesto";

-- (6) Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8



-- (7) lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas


-- (8) Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa



-- (9) Listar el nombre de los puertos cuya altura sea mayor de 1500 



-- (10) Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000



-- (11) Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000